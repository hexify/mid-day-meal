;(function() {
	let currentAction = 0
	let currentAnim = 0
	let currentSlide = 0

	let nextAction = () => {
		if (currentAction < actions.length)
			actions[currentAction++] === 'nextSlide' ? nextSlide() : nextAnim()
	}

	let nextAnim = () => document.body.classList.add('anim' + currentAnim++)

	let nextSlide = () => {
		document
			.getElementById('slide' + ++currentSlide)
			.classList.add('slide--done')
		setTimeout(() => {
			document.body.style.transform = 'translateY(-' + currentSlide * 100 + '%)'
		}, 300)
	}

	let prevAction = () => {
		console.log(actions[currentAction])
		if (currentAction < actions.length)
			actions[--currentAction] === 'nextSlide' ? prevSlide() : prevAnim()
	}

	let prevSlide = () => {
		document
			.getElementById('slide' + currentSlide--)
			.classList.remove('slide--done')
		setTimeout(() => {
			document.body.style.transform = 'translateY(-' + currentSlide * 100 + '%)'
		}, 300)
	}

	let prevAnim = () => document.body.classList.remove('anim' + --currentAnim)

	let actions = [
		'nextSlide',
		'nextAnim',
		'nextAnim',
		'nextAnim',
		'nextSlide',
		'nextAnim'
	]

	document.addEventListener('keydown', event => {
		if (event.keyCode === 27) window.scrollTo(0, 0)
		if (event.isComposing || event.keyCode === 229) return
		if (event.keyCode === 33 || event.keyCode === 37) prevAction()
		if (event.keyCode === 34 || event.keyCode === 39) nextAction()
	})
})()
