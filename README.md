# Mid Day Meal

Helping the Mid Day Meal committee in schools create the menu that the students need based on nutrient deficiency information from Hospitals.

Mid Day Meal Programme is one of the most important programmes of the Government of India to encourage children to come to schools and take part in the learning process without worrying for their meal.

Mid-Day Meal Scheme is managed, monitored and supervised at school level by the "School Mid Day Meal Committee" which consists of PTA President, members of Mother PTA, representatives of Parents of children belonging to SC/ST category and minority communities, Ward Member, Head of the institution and Teachers’ representatives. The committee has various responsibilities including deciding what menu is. The committee is convened once in every month to decide on the menu. The committee also knows the nutrition value of the food that will be served.

Kerala runs medical hospital (PHC, Taluk level / District level hospitals) which will have records on the disease and cause of the same for children who visit these PHC and hospitals. From these records we can determine the nutrient deficiency seen in those children on a real-time basis.

Our main objective is to create a way to send this information in real-time to the Mid Day Meal Committee in schools. This will assisst the committee in taking the right decisions about the menu.

# Goals

- Create a way for hospitals to inform the schools nearby about the deficiency of nutrients that are seen in children that show up at the hospitals.
- Enable the committees to stay informed about the deficiency of nutrients that are seen in children that show up at the hospitals.
- Enable the committees to manage the Mid Day Meal menu and ensure it meets the nutrient requirements.

# Solutions

1. A portal for hospital staff to upload details of nutrient deficiencies seen in the children that visit the hospital.
2. Display the information collected in hospitals in a way that is useful for the Mid Day Meal organizing committees, i.e., nutrient deficiencies that have been reported last month, progress made by comparing current statistics with past statistics.
3. A Mid Day Meal Menu management system that shows the menu and the nutrient values of each meal.

# Users

1. Admin
2. Expert (Health Expert)
3. Official (Govt. Official)
4. School
5. Supplier

# Modules

0. Homepage to navigate to user login.

## Admin

1. Login for system admin.
2. List of all users.
3. Update user.
4. Disable user.

## Expert

1. Views monthly statistics.
2. Create recommendations.

## Official

1. Views approved orders from schools.
2. Forwards orders to supplier.
3. Views invoices.

## School

1. Views orders from expert.
2. Approves order.

## Supplier

1. Views orders from official.
2. Sends supplies to school and invoice to official.

# Schema

## Admin

- email (string)
- password (string)

## Expert (Health Expert)

- email (string)
- password (string)
- name (string)
- active (number, [0: inactive, 1: active])

## Health Data

- student (student.id)
- datetime (string)
- nutrients (array)

## Official (Govt. Official)

- email (string)
- password (string)
- name (string)
- district (string)
- active (number, [0: inactive, 1: active])

## Order

- items (array)
- datetime (string)
- status (number, [0: generated, 1: approved, 2: forwarded, 3: supplied])
- expert (expert.id)
- school (school.id)
- official (official.id)
- supplier (supplier.id)

## School

- email (string)
- password (string)
- name (string)
- district (string)
- active (number, [0: inactive, 1: active])

## Student

- name (string)
- school (school.id)
- active (number, [0: inactive, 1: active])

## Supplier

- email (string)
- password (string)
- name (string)
- district (string)
- active (number, [0: inactive, 1: active])

# API

## Admin

- post: /api/admin/ (login admin)

## Expert (Health Expert)

- get:  /api/expert/     ( get all experts )
- get:  /api/expert/:id  ( get specific expert )
- post: /api/expert/     ( login expert    )
- post: /api/expert/new/ ( add new expert  )
- put:  /api/expert/:id  ( update expert   )

## Health Data

- get:  /api/health/ ( get statistics )
- get:  /api/health/:id (get specific health data)
- post: /api/health/ ( add data       )

## Official (Govt. Official)

- get:  /api/official/     ( get all officials )
- get:  /api/official/:id  ( get specific official )
- post: /api/official/     ( login official    )
- post: /api/official/new/ ( add new official  )
- put:  /api/official/:id  ( update official   )

## Order

- get:  /api/order/:status/:id ( get orders    )
- post: /api/order/            ( add new order )
- put:  /api/order/:id         ( update order  )

## School

- get:  /api/school/     ( get all schools )
- get:  /api/school/:id  ( get specific school )
- post: /api/school/     ( login school    )
- post: /api/school/new/ ( add new school  )
- put:  /api/school/:id  ( update school   )

## Student

- get:  /api/student/:id  ( get no of students )
- post: /api/student/new/ ( add new student    )

## Supplier

- get:  /api/supplier/     ( get all suppliers )
- get   /api/supplier/:id  ( get specific supplier )
- post: /api/supplier/     ( login supplier    )
- post: /api/supplier/new/ ( add new supplier  )
- put:  /api/supplier/:id  ( update supplier   )

# Suggested Changes

## Review 0, 28th @ 02:00 PM

- Food expert needed for food menu preparations.
- Show statistics and actions taken by schools to district officers.
- Validate that schools are following correct food chart/menu.
- Access for government officials to view statistics and report.
- Hospital data should include school name, place & district.
- If possible, find the details of data collected during a health checkup.
- Impact for environment + collaboration with other department.

## Review 1, 28th @ 07:00 PM

- Government commenting; How does a followup work?
- Supplies Order: Provision to calculate quantity of items needed for a month (should be transparent).
- Student data can be entered from both school (through health checkup in schools) and through hospital.
- Food chart can be prepared by both schools and government officials, but it should be transparent.
- Dynamically adding new roles like suppliers.
- Add Viewable documentation.

## Review 2, 28th @ 09:30 PM

- Schools may receive wrong data. Schools need to send back some messages.
- Orders (like quantity of items for a month) can be autogenerated.
- Giving quotation to parties other than supplico.
