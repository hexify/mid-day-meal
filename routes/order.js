const express = require('express')

const auth = require('../middleware/auth')
const Order = require('../models/Order')
const router = express.Router()

router.get('/:status/:id', auth, async (req, res) => {
    try {
        let ids = ["school", "order", "supplier"]
        let orders = await Order.find({ status: req.params.status, [ids[req.params.status]]: req.params.id })
        res.json(orders);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.get('/expert/:id', auth, async (req, res) => {
    try {
        let orders = await Order.find({ expert: req.params.id })
        res.json(orders);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/', auth, async (req, res) => {
    const { items, dateTime } = req.body
    try {
        let order = new Order({
            items: items,
            dateTime: dateTime,
            status: 0
        })
        let { id } = await order.save();
        res.send(id);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.put('/:id', auth, async (req, res) => {
    const { items, dateTime, status, expert, school, official, supplier } = req.body

    try {
        let order = await Order.findById(req.param.id);
        if (order === null)
            return res.status(401).json({ msg: 'Order doesn\'t exist' })

        order = {}
        if (items) order.items = items
        if (dateTime) order.dateTime = dateTime
        if (status) order.status = status
        if (expert) order.expert = expert
        if (school) order.school = school
        if (official) order.official = official
        if (supplier) order.supplier = supplier

        await order.findByIdAndUpdate(
            req.params.id,
            { $set: order },
            { new: true }
        )

        res.send({ msg: 'Updated' })
    } catch (err) {
        console.error(err)
        res.status(500).send('Server Error')
    }
})

module.exports = router