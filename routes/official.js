const bcrypt = require('bcryptjs')
const express = require('express')
const jwt = require('jsonwebtoken')

const auth = require('../middleware/auth')
const Official = require('../models/Official')
const router = express.Router()

router.get('/', auth, async (req, res) => {
    try {
        let officials = await Official.find()
        res.json(officials);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.get('/:id', auth, async (req, res) => {
    try {
        let official = await Official.findById(req.params.id)
        res.json(official)
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/', [], async (req, res) => {
    const { email, password } = req.body

    try {
        let official = await Official.findOne({ email })

        if (official) {
            const isMatch = await bcrypt.compare(password, official.password)
            if (!isMatch) {
                return res.status(400).json({ msg: 'Password does not match' })
            }
        }

        if (official) {
            const payload = {
                official: {
                    id: official.id
                }
            }

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                {
                    expiresIn: 86400
                },
                (err, token) => {
                    if (err) throw err
                    res.json({ token, email: official.email })
                }
            )
        } else {
            return res.status(400).json({ msg: 'Email does not exist' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server Error')
    }
})

router.post('/new', auth, async (req, res) => {
    const { email, password, name, district } = req.body
    try {
        let offcial = await Official.findOne({ email })
        if (offcial !== null)
            return res.status(401).json({ msg: 'Email already in use' })
        else {
            official = new Official({
                email,
                name,
                district,
                active: 1
            })
            const salt = await bcrypt.genSalt(10)
            official.password = await bcrypt.hash(password, salt)
            let { id } = await official.save();
            res.send(id);
        }
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.put('/:id', auth, async (req, res) => {
    const { email, password, name, district, active } = req.body

    try {
        let official = await Official.findById(req.params.id);
        if (official === null)
            return res.status(401).json({ msg: 'Official doesn\'t exist' })

        official = {}
        if (email) official.email = email
        if (password) {
            const salt = await bcrypt.genSalt(10)
            official.password = await bcrypt.hash(password, salt)
        }
        if (name) official.name = name
        if (district) official.district = district
        if (active) official.active = active

        await Official.findByIdAndUpdate(
            req.params.id,
            { $set: official },
            { new: true }
        )

        res.send({ msg: 'Updated' })
    } catch (err) {
        console.error(err)
        res.status(500).send('Server Error')
    }
})

module.exports = router