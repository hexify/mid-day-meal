const bcrypt = require('bcryptjs')
const express = require('express')
const jwt = require('jsonwebtoken')

const auth = require('../middleware/auth')
const School = require('../models/School')
const router = express.Router()

router.get('/', auth, async (req, res) => {
    try {
        let schools = await School.find()
        res.json(schools);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.get('/:id', auth, async (req, res) => {
    try {
        let school = await School.findById(req.params.id)
        res.json(school)
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/', [], async (req, res) => {
    const { email, password } = req.body

    try {
        let school = await School.findOne({ email })

        if (school) {
            const isMatch = await bcrypt.compare(password, school.password)
            if (!isMatch) {
                return res.status(400).json({ msg: 'Password does not match' })
            }
        }

        if (school) {
            const payload = {
                school: {
                    id: school.id
                }
            }

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                {
                    expiresIn: 86400
                },
                (err, token) => {
                    if (err) throw err
                    res.json({ token, email: school.email })
                }
            )
        } else {
            return res.status(400).json({ msg: 'Email does not exist' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server Error')
    }
})

router.post('/new', auth, async (req, res) => {
    const { email, password, name, district } = req.body
    try {
        let school = await School.findOne({ email })
        if (school !== null)
            return res.status(401).json({ msg: 'Email already in use' })
        else {
            school = new School({
                email,
                name,
                district,
                active: 1
            })
            const salt = await bcrypt.genSalt(10)
            school.password = await bcrypt.hash(password, salt)
            let { id } = await school.save();
            res.send(id);
        }
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.put('/:id', auth, async (req, res) => {
    const { email, password, name, district, active } = req.body

    try {
        let school = await School.findById(req.params.id);
        if (school === null)
            return res.status(401).json({ msg: 'School doesn\'t exist' })

        school = {}
        if (email) school.email = email
        if (password) {
            const salt = await bcrypt.genSalt(10)
            school.password = await bcrypt.hash(password, salt)
        }
        if (name) school.name = name
        if (district) school.district = district
        if (active) school.active = active

        await School.findByIdAndUpdate(
            req.params.id,
            { $set: school },
            { new: true }
        )

        res.send({ msg: 'Updated' })
    } catch (err) {
        console.error(err)
        res.status(500).send('Server Error')
    }
})

module.exports = router