const express = require('express')

const auth = require('../middleware/auth')
const Student = require('../models/Student')
const router = express.Router()

router.get('/:id', auth, async (req, res) => {
    try {
        let students = await Student.find({ school: req.param.id })
        res.json(students.length);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/new', auth, async (req, res) => {
    const { name, school, active } = req.body
    try {
        let student = new Student({
            name,
            school,
            active
        })
        let { id } = await student.save();
        res.send(id);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

module.exports = router