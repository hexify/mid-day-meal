const bcrypt = require('bcryptjs')
const express = require('express')
const jwt = require('jsonwebtoken')
const config = require('config')

const Admin = require('../models/Admin')
const router = express.Router()

router.post('/', [], async (req, res) => {
	const { email, password } = req.body

	try {
		let admin = await Admin.findOne({ email })

		if (admin) {
			const isMatch = await bcrypt.compare(password, admin.password)
			if (!isMatch) {
				return res.status(400).json({ msg: 'Password does not match' })
			}
		}

		if (admin) {
			const payload = {
				admin: {
					id: admin.id
				}
			}

			jwt.sign(
				payload,
				config.get('jwtSecret'),
				{
					expiresIn: 86400
				},
				(err, token) => {
					if (err) throw err
					res.json({ token, email: admin.email })
				}
			)
		} else {
			return res.status(400).json({ msg: 'Email does not exist' })
		}
	} catch (err) {
		console.error(err.message)
		res.status(500).send('Server Error')
	}
})

module.exports = router
