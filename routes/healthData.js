const express = require('express')

const auth = require('../middleware/auth')
const Health = require('../models/Health')
const router = express.Router()

router.get('/', auth, async (req, res) => {
    try {
        let healthDatas = await Health.find()
        res.json(healthDatas);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.get('/:id', auth, async (req, res) => {
    try {
        let health = await Health.findById(req.params.id);
        res.json(health)
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/', auth, async (req, res) => {
    const { student, dateTime, nutrients } = req.body
    try {
        let HealthData = new HealthData({
            student,
            dateTime,
            nutrients
        })
        let { id } = await healthData.save();
        res.send(id);
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

module.exports = router