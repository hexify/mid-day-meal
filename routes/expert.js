const bcrypt = require('bcryptjs')
const express = require('express')

const auth = require('../middleware/auth')
const Expert = require('../models/Expert')
const School = require('../models/School')
const Order = require('../models/Order')
const router = express.Router()

router.get('/', auth, async (req, res) => {
	try {
		let experts = await Expert.find().select('-password')
		res.json(experts)
	} catch (err) {
		console.log(err)
		res.status(500).send('Server Error')
	}
})

router.get('/:id', auth, async (req, res) => {
    try {
        let expert = await Expert.findById(req.param.id)
        res.json(expert)
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/new', auth, async (req, res) => {
	const { email, password, name } = req.body
	try {
		let expert = await Expert.findOne({ email })
		if (expert !== null)
			return res.status(401).json({ msg: 'Email already in use' })
		else {
			expert = new Expert({
				email,
				name,
				active: 1
			})
			const salt = await bcrypt.genSalt(10)
			expert.password = await bcrypt.hash(password, salt)
			let { id } = await expert.save()
			res.send(id)
		}
	} catch (err) {
		console.log(err)
		res.status(500).send('Server Error')
	}
})

router.post('/', [], async (req, res) => {
	const { email, password } = req.body

	try {
		let expert = await Expert.findOne({ email })

		if (expert) {
			const isMatch = await bcrypt.compare(password, expert.password)
			if (!isMatch) {
				return res.status(400).json({ msg: 'Password does not match' })
			}
		}

		if (expert) {
			const payload = {
				user: {
					id: expert.id
				}
			}

			jwt.sign(
				payload,
				config.get('jwtSecret'),
				{
					expiresIn: 86400
				},
				(err, token) => {
					if (err) throw err
					res.json({ token, email: expert.email })
				}
			)
		} else {
			return res.status(400).json({ msg: 'Email does not exist' })
		}
	} catch (err) {
		console.error(err.message)
		res.status(500).send('Server Error')
	}
})

router.post('/additems', auth, async (req,res) => {
	const {district, items} = req.body
	try{
		let schools = await School.find({district})
		schools.some(element => {
			let order = new Order({
				items: JSON.stringify(items),
				expert: req.admin.id,
				School: element._id
			})
			order.save()
		})
		res.json({ msg: 'Report saved' })
	}
	catch(err){
		console.log(err)
		res.status(500).send('Server Error')
	}
})

router.put('/:id', auth, async (req, res) => {
	const { email, password, name, active } = req.body
	try {
		let expert = await Expert.findById(req.params.id)
		if (expert === null)
			return res.status(401).json({ msg: "Expert doesn't exist" })

		expert = {}
		if (email) expert.email = email
		if (password) {
			const salt = await bcrypt.genSalt(10)
			expert.password = await bcrypt.hash(password, salt)
		}
		if (name) expert.name = name
		if (active) expert.active = active

		await Expert.findByIdAndUpdate(
			req.params.id,
			{ $set: expert },
			{ new: true }
		)

		res.send({ msg: 'Updated' })
	} catch (err) {
		console.error(err)
		res.status(500).send('Server Error')
	}
})

module.exports = router
