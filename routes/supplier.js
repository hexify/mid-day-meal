const bcrypt = require('bcryptjs')
const express = require('express')
const jwt = require('jsonwebtoken')

const auth = require('../middleware/auth')
const Supplier = require('../models/Supplier')
const router = express.Router()

router.get('/', auth, async (req, res) => {
	try {
		let suppliers = await Supplier.find()
		res.json(suppliers)
	} catch (err) {
		console.log(err)
		res.status(500).send('Server Error')
	}
})

router.get('/:id', auth, async (req, res) => {
    try {
        let supplier = await Supplier.findById(req.params.id)
        res.json(supplier)
    }
    catch (err) {
        console.log(err)
        res.status(500).send('Server Error')
    }
})

router.post('/', [], async (req, res) => {
    const { email, password } = req.body

    try {
        let supplier = await Supplier.findOne({ email })

        if (supplier) {
            const isMatch = await bcrypt.compare(password, supplier.password)
            if (!isMatch) {
                return res.status(400).json({ msg: 'Password does not match' })
            }
        }

        if (supplier) {
            const payload = {
                supplier: {
                    id: supplier.id
                }
            }

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                {
                    expiresIn: 86400
                },
                (err, token) => {
                    if (err) throw err
                    res.json({ token, email: supplier.email })
                }
            )
        } else {
            return res.status(400).json({ msg: 'Email does not exist' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server Error')
    }
})

router.post('/new', auth, async (req, res) => {
	const { email, password, name, district } = req.body
	try {
		let supplier = await Supplier.findOne({ email })
		if (supplier !== null)
			return res.status(401).json({ msg: 'Email already in use' })
		else {
			supplier = new Supplier({
				email,
				name,
				district,
				active: 1
			})
			const salt = await bcrypt.genSalt(10)
			supplier.password = await bcrypt.hash(password, salt)
			let { id } = await supplier.save()
			res.send(id)
		}
	} catch (err) {
		console.log(err)
		res.status(500).send('Server Error')
	}
})

router.put('/:id', auth, async (req, res) => {
	const { email, password, name, district, active } = req.body

	try {
		let supplier = await Supplier.findById(req.params.id)
		if (supplier === null)
			return res.status(401).json({ msg: "Supplier doesn't exist" })

		supplier = {}
		if (email) supplier.email = email
		if (password) {
			const salt = await bcrypt.genSalt(10)
			supplier.password = await bcrypt.hash(password, salt)
		}
		if (name) supplier.name = name
		if (district) supplier.district = district
		if (active) supplier.active = active

		await Supplier.findByIdAndUpdate(
			req.params.id,
			{ $set: supplier },
			{ new: true }
		)

		res.send({ msg: 'Updated' })
	} catch (err) {
		console.error(err)
		res.status(500).send('Server Error')
	}
})

module.exports = router
