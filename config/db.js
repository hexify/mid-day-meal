const config = require('config')
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

const db = config.get('mongoURI')

const Admin = require('../models/Admin')

const connectDB = async () => {
	try {
		await mongoose.connect(db, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true
		})
		console.log('MongoDB Connected...')

		// let expert = new Admin({
		// 	email: 'test@123'
		// })
		// const salt = await bcrypt.genSalt(10)
		// expert.password = await bcrypt.hash('password', salt)
		// await expert.save();
		// console.log('Admin added')
	} catch (err) {
		console.error(err.message)
		process.exit(1)
	}
}

module.exports = connectDB
