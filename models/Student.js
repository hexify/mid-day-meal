const mongoose = require('mongoose')

const StudentSchema = mongoose.Schema({
	name: {
		type: String
	},
	school: {
		type: mongoose.Schema.Types.ObjectId
	},
	active: {
		type: Number
	}
})

module.exports = mongoose.model('student', StudentSchema)
