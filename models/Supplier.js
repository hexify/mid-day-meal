const mongoose = require('mongoose')

const SupplierSchema = mongoose.Schema({
	email: {
		type: String
	},
	password: {
		type: String
	},
	name: {
		type: String
	},
	district: {
		type: String
	},
	active: {
		type: Number
	}
})

module.exports = mongoose.model('supplier', SupplierSchema)
