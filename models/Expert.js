const mongoose = require('mongoose')

const ExpertSchema = mongoose.Schema({
	email: {
		type: String
	},
	password: {
		type: String
	},
	name: {
		type: String
	},
	
	active: {
		type: Number
	}
})

module.exports = mongoose.model('expert', ExpertSchema)
