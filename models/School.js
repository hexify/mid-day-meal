const mongoose = require('mongoose')

const SchoolSchema = mongoose.Schema({
	email: {
    type: String
    },
	password: {
		type: String
    },
    name: {
        type: String
    },
    district: {
        type: String
    },
    active: {
        type: Number
    },
	nutrients: {
		type: Array
    },
    menu: {
        type: JSON
    },
    items: {
        type: JSON
    }
})

module.exports = mongoose.model('school', SchoolSchema)
