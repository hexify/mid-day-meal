const mongoose = require('mongoose')

const HealthSchema = mongoose.Schema({
	student: {
		type: mongoose.Schema.Types.ObjectId
	},
	dateTime: {
		type: String,
		default: Date.now
	},
	nutrients: {
		type: Array
	}
})

module.exports = mongoose.model('health', HealthSchema)
