const mongoose = require('mongoose')

const OfficialSchema = mongoose.Schema({
	email: {
		type: String
	},
	password: {
		type: String
	},
	name: {
		type: String
	},
	district: {
		type: String
	},
	active: {
		type: Number
	}
})

module.exports = mongoose.model('official', OfficialSchema)
