const mongoose = require('mongoose')

const OrderSchema = mongoose.Schema({
	items: {
		type: JSON
	},
	status: {
		type: Number,
		default: 0
	},
	expert: {
		type: mongoose.Schema.Types.ObjectId
	},
	school: {
		type: mongoose.Schema.Types.ObjectId
	},
	official: {
		type: mongoose.Schema.Types.ObjectId
	},
	supplier: {
		type: mongoose.Schema.Types.ObjectId
	},
	dateTime: {
		type: String,
		default: Date.now
	}
})

module.exports = mongoose.model('order', OrderSchema)
