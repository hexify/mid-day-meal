import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import AdminAdd from './components/admin/Add'
import AdminEdit from './components/admin/Edit'
import AdminList from './components/admin/List'
import AdminLogin from './components/admin/Login'
import ExpertHome from './components/expert/Home'
import ExpertLogin from './components/expert/Login'
import ExpertReport from './components/expert/Report'
import OfficialHome from './components/official/Home'
import OfficialLogin from './components/official/Login'
import SchoolHome from './components/school/Home'
import SchoolLogin from './components/school/Login'
import SupplierLogin from './components/supplier/Login'
import Home from './components/page/Home'
import store from './store'
import './App.css'

const App = () => (
	<Provider store={store}>
		<Router>
			<Switch>
				<Route exact path='/' component={Home} />
				<Route exact path='/admin' component={AdminList} />
				<Route exact path='/admin/add' component={AdminAdd} />
				<Route exact path='/admin/edit/:type/:id' component={AdminEdit} />
				<Route exact path='/admin/login' component={AdminLogin} />
				<Route exact path='/expert' component={ExpertHome} />
				<Route exact path='/expert/login' component={ExpertLogin} />
				<Route exact path='/expert/report' component={ExpertReport} />
				<Route exact path='/official' component={OfficialHome} />
				<Route exact path='/official/login' component={OfficialLogin} />
				<Route exact path='/school' component={SchoolHome} />
				<Route exact path='/school/login' component={SchoolLogin} />
				<Route exact path='/supplier/login' component={SupplierLogin} />
			</Switch>
		</Router>
	</Provider>
)

export default App
