import React from 'react'
import {
	Button,
	Card,
	CardActions,
	CardContent,
	Chip,
	Grid,
	Typography,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Paper
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const orders = [
	{
		district: 'Alappuzha',
		items: [
			{ name: 'Dal', quantity: 20 },
			{ name: 'Green Peas', quantity: 20 }
		]
	}
]

const useStyles = makeStyles(theme => ({
	card: {
		color: theme.palette.text.secondary
	},
	cardButton: {
		color: theme.palette.text.secondary
	},
	heading1: {
		margin: '30px 0px 24px 0px'
	},
	heading12: {
		margin: '120px 0px 24px 0px'
	},
	heading2: {
		margin: '60px 0px 24px 0px'
	},
	root: {
		margin: '0px',
		width: 'calc(100% - ' + theme.spacing(3) / 2 + 'px)'
	},
	table: {
		minWidth: 650
	}
}))

const List = () => {
	const classes = useStyles()

	let columns = [
		['Amaranth', 'Spinach', 'Drumstick leaf', 'Other edible green leaves'],
		['Beetroot', 'Onions', 'Tomato', 'Carrot Pumpkin'],
		['Green gram', 'Red gram', 'Bengal Gram', 'Tur dal'],
		['Cauliflower', 'Potato', 'Radish', 'Ash gourd Cucumber'],
		['Ladies finger', 'Green peas', 'Beans', 'Bitter gourd Bottle gourd']
	]

	return (
		<>
			<Typography variant='h2' component='h1' className={classes.heading1}>
				School Chart
			</Typography>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label='simple table'>
					<TableHead>
						<TableRow>
							<TableCell>Monday</TableCell>
							<TableCell>Tuesday</TableCell>
							<TableCell>Wednesday</TableCell>
							<TableCell>Thursday</TableCell>
							<TableCell>Friday</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						<TableRow>
							{columns.map((column, key) => (
								<TableCell key={key}>
									{column.map((item, key2) => (
										<Chip label={item} key={key2} variant='outlined' />
									))}
								</TableCell>
							))}
						</TableRow>
					</TableBody>
				</Table>
			</TableContainer>
			<Typography variant='h2' component='h1' className={classes.heading12}>
				School Orders
			</Typography>
			<Grid container spacing={3} className={classes.root}>
				{orders.map(({ district, items }, key) => (
					<Grid item xs={4} key={key}>
						<Card className={classes.card}>
							<CardContent>
								<Typography variant='h4' component='h2'>
									{district}
								</Typography>
								{items.map(({ name, quantity }, key2) => (
									<p key={key2}>
										{name} - {quantity}
									</p>
								))}
							</CardContent>
							<CardActions>
								<Button className={classes.cardButton} size='small'>
									Approve
								</Button>
							</CardActions>
						</Card>
					</Grid>
				))}
			</Grid>
			<Typography variant='h4' component='h2' className={classes.heading2}>
				Approved Orders
			</Typography>
			<Grid container spacing={3} className={classes.root}>
				{orders.map(({ district, items }, key) => (
					<Grid item xs={4} key={key}>
						<Card className={classes.card}>
							<CardContent>
								<Typography variant='h4' component='h2'>
									{district}
								</Typography>
								{items.map(({ name, quantity }, key2) => (
									<p key={key2}>
										{name} - {quantity}
									</p>
								))}
							</CardContent>
						</Card>
					</Grid>
				))}
			</Grid>
		</>
	)
}

export default List
