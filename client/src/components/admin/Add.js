import React, { useState, useEffect } from 'react'
import {
	FormControl,
	Grid,
	Input,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'

import { addUser, adminClearError } from '../../actions/adminActions'

const districts = [
	'Alappuzha',
	'Ernakulam',
	'Idukki',
	'Kannur',
	'Kollam',
	'Kasaragod',
	'Kottayam',
	'Kozhikode',
	'Malappuram',
	'Palakkad',
	'Pathanamthitta',
	'Thiruvananthapuram',
	'Thrissur',
	'Wayanadu'
]

const types = ['Expert', 'Official', 'School', 'Supplier']

const useStyles = makeStyles(theme => ({
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120
	},
	root: {
		alignContent: 'center',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		height: '100%',
		textAlign: 'center'
	},
	heading: {
		marginBottom: '36px'
	}
}))

const Add = ({ addUser, admin: { alert }, adminClearError, history }) => {
	const initialState = {
		email: '',
		password: '',
		confirmPassword: '',
		name: '',
		type: '',
		district: null
	}

	const [user, setUser] = useState(initialState)

	const classes = useStyles()

	useEffect(() => {
		if (alert === 'User added!') {
			adminClearError()
			history.push('/admin/')
		}
		// eslint-disable-next-line
	}, [alert])

	const inputHandler = ({ target }) => {
		setUser({ ...user, [target.name]: target.value })
	}

	const inputHandlerType = ({ target }) => {
		if (target.value === 'Expert')
			setUser({ ...user, [target.name]: target.value, district: null })
		else setUser({ ...user, [target.name]: target.value, district: '' })
	}

	const submitHandler = event => {
		event.preventDefault()
		addUser({
			email: user.email,
			password: user.password,
			type: user.type,
			district: user.district,
			name: user.name
		})
	}

	return (
		<>
			<Grid className={classes.root} container spacing={0}>
				<Typography variant='h4' component='h1' className={classes.heading}>
					Add User
				</Typography>
				<form noValidate autoComplete='off' onSubmit={submitHandler}>
					<TextField
						name='email'
						label='Email'
						variant='outlined'
						autoFocus
						value={user.email}
						onChange={inputHandler}
					/>
					<br />
					<br />
					<TextField
						name='password'
						label='Password'
						type='password'
						variant='outlined'
						value={user.password}
						onChange={inputHandler}
					/>
					<br />
					<br />
					<TextField
						name='confirmPassword'
						label='Confirm Password'
						type='password'
						variant='outlined'
						value={user.confirmPassword}
						onChange={inputHandler}
					/>
					<br />
					<br />
					<TextField
						name='name'
						label='Name'
						variant='outlined'
						value={user.name}
						onChange={inputHandler}
					/>
					<br />
					<br />
					<FormControl className={classes.formControl}>
						<InputLabel id='demo-simple-select-outlined-label'>Type</InputLabel>
						<Select
							name='type'
							labelId='demo-simple-select-outlined-label'
							id='demo-simple-select-outlined'
							value={user.type}
							onChange={inputHandlerType}
						>
							{types.map((type, key) => (
								<MenuItem value={type} key={key}>
									{type}
								</MenuItem>
							))}
						</Select>
					</FormControl>
					<br />
					<br />
					{user.district !== null ? (
						<>
							<FormControl className={classes.formControl}>
								<InputLabel id='demo-simple-select-outlined-label'>
									District
								</InputLabel>
								<Select
									name='district'
									labelId='demo-simple-select-outlined-label'
									id='demo-simple-select-outlined'
									value={user.district}
									onChange={inputHandler}
								>
									{districts.map((district, key) => (
										<MenuItem value={district} key={key}>
											{district}
										</MenuItem>
									))}
								</Select>
							</FormControl>
							<br />
							<br />
						</>
					) : (
						<></>
					)}
					<Input type='submit' value='Add User' />
				</form>
			</Grid>
		</>
	)
}

const mapStateToProps = state => ({
	admin: state.admin
})

export default connect(mapStateToProps, { addUser, adminClearError })(Add)
