import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import {
	Card,
	CardActions,
	CardContent,
	Button,
	Grid,
	Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'

import { getList } from '../../actions/adminActions'

const useStyles = makeStyles(theme => ({
	card: {
		color: theme.palette.text.secondary
	},
	cardButton: {
		color: theme.palette.text.secondary
	},
	root: {
		margin: '0px',
		width: 'calc(100% - ' + theme.spacing(3) / 2 + 'px)'
	}
}))

const List = ({ admin: { users }, getList }) => {
	useEffect(() => {
		getList()
		// eslint-disable-next-line
	}, [])

	const classes = useStyles()

	return (
		<>
			<Typography variant='h2' component='h1'>
				User Accounts
			</Typography>
			<Link to='/admin/add'>
				<Button
					variant='outlined'
					color='primary'
					className={classes.cardButton}
					size='small'
				>
					Add
				</Button>
			</Link>
			<Grid container spacing={3} className={classes.root}>
				{users.length > 0 ? (
					users.map(({ _id, name, email, type }, key) => (
						<Grid item xs={4} key={key}>
							<Card className={classes.card}>
								<CardContent>
									<Typography variant='h4' component='h2'>
										{name}
									</Typography>
									<p>{email}</p>
									<p>{type}</p>
								</CardContent>
								<CardActions>
									<Link to={'/admin/edit/' + type + '/' + _id}>
										<Button className={classes.cardButton} size='small'>
											Edit
										</Button>
									</Link>
									<Button className={classes.cardButton} size='small'>
										Disable
									</Button>
								</CardActions>
							</Card>
						</Grid>
					))
				) : (
					<Typography variant='h4' component='h2'>
						No Users
					</Typography>
				)}
			</Grid>
		</>
	)
}

const mapStateToProps = state => ({
	admin: state.admin
})

export default connect(mapStateToProps, { getList })(List)
