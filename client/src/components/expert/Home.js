import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import {
	Button,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Paper,
	Select,
	Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const districts = [
	'Alappuzha',
	'Ernakulam',
	'Idukki',
	'Kannur',
	'Kollam',
	'Kasaragod',
	'Kottayam',
	'Kozhikode',
	'Malappuram',
	'Palakkad',
	'Pathanamthitta',
	'Thiruvananthapuram',
	'Thrissur',
	'Wayanadu'
]

const nutrients = [
	{ name: 'VitB', cases: 29 },
	{ name: 'VitA', cases: 23 },
	{ name: 'Iron', cases: 7 },
	{ name: 'Zinc', cases: 3 }
]

const recommendations = [
	{
		district: 'Alappuzha',
		items: [
			{ name: 'Dal', quantity: 20 },
			{ name: 'Green Peas', quantity: 20 }
		]
	}
]

const useStyles = makeStyles(theme => ({
	form: {
		alignItems: 'center',
		display: 'flex',
		justifyContent: 'center',
		margin: '24px 0px'
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120
	},
	heading2: {
		margin: '60px 0px 24px 0px'
	},
	paper: {
		padding: theme.spacing(2)
	},
	root: {
		textAlign: 'center'
	}
}))

const Home = () => {
	const initialState = {
		district: '',
		month: 3,
		year: 2020
	}

	const [date, setDate] = useState(initialState)

	const classes = useStyles()

	const inputHandler = ({ target }) => {
		setDate({
			...date,
			[target.name]: target.value
		})
	}

	return (
		<div className={classes.root}>
			<Typography variant='h2' component='h1'>
				Statistics
			</Typography>
			<form
				className={classes.form}
				noValidate
				autoComplete='off'
				onSubmit={event => event.preventDefault()}
			>
				<FormControl className={classes.formControl}>
					<InputLabel id='demo-simple-select-outlined-label'>
						District
					</InputLabel>
					<Select
						name='district'
						labelId='demo-simple-select-outlined-label'
						id='demo-simple-select-outlined'
						value={date.district}
						onChange={inputHandler}
					>
						{districts.map((district, key) => (
							<MenuItem value={district} key={key}>
								{district}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<FormControl className={classes.formControl}>
					<InputLabel id='demo-simple-select-outlined-label'>Month</InputLabel>
					<Select
						name='month'
						labelId='demo-simple-select-outlined-label'
						id='demo-simple-select-outlined'
						value={date.month}
						onChange={inputHandler}
					>
						<MenuItem value={3}>
							<em>March</em>
						</MenuItem>
						<MenuItem value={2}>February</MenuItem>
						<MenuItem value={1}>January</MenuItem>
					</Select>
				</FormControl>
				<FormControl className={classes.formControl}>
					<InputLabel id='demo-simple-select-outlined-label'>Year</InputLabel>
					<Select
						name='year'
						labelId='demo-simple-select-outlined-label'
						id='demo-simple-select-outlined'
						value={date.year}
						onChange={inputHandler}
					>
						<MenuItem value={2020}>
							<em>2020</em>
						</MenuItem>
					</Select>
				</FormControl>
				<Link to='/expert/report'>
					<Button variant='contained'>Send Recommendation</Button>
				</Link>
			</form>
			<Grid container spacing={3}>
				{nutrients.map(({ name, cases }, key) => (
					<Grid item xs={4} key={key}>
						<Paper className={classes.paper}>
							{name} ({cases})
						</Paper>
					</Grid>
				))}
			</Grid>
			<Typography variant='h4' component='h2' className={classes.heading2}>
				Unapproved Recommendations
			</Typography>
			<Grid container spacing={3}>
				{recommendations.map(({ district, items }, key) => (
					<Grid item xs={4} key={key}>
						<Paper className={classes.paper}>
							<Typography variant='h6' component='h3'>
								{district}
							</Typography>
							{items.map(({ name, quantity }, key2) => (
								<p key={key2}>
									{name} - {quantity}
								</p>
							))}
						</Paper>
					</Grid>
				))}
			</Grid>
		</div>
	)
}

export default Home
