import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import {
	Button,
	FormControl,
	Grid,
	Input,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { expertAddReport, expertClearError } from '../../actions/expertActions'

const districts = [
	'Alappuzha',
	'Ernakulam',
	'Idukki',
	'Kannur',
	'Kollam',
	'Kasaragod',
	'Kottayam',
	'Kozhikode',
	'Malappuram',
	'Palakkad',
	'Pathanamthitta',
	'Thiruvananthapuram',
	'Thrissur',
	'Wayanadu'
]

const useStyles = makeStyles(theme => ({
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120
	},
	root: {
		alignContent: 'center',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		height: '100%',
		textAlign: 'center'
	},
	heading: {
		marginBottom: '36px'
	}
}))

const Report = ({ history, expertAddReport, expert: { alert }, expertClearError }) => {
	const initialState = {
		district: districts[0],
		items: [{ name: '', quantity: 0 }]
	}

	const [recom, setRecom] = useState(initialState)

	const classes = useStyles()

	useEffect(() => {
		if (alert === 'Report added') {
			expertClearError()
			history.push('/admin/')
		}
		// eslint-disable-next-line
	}, [alert])

	const addItem = () => {
		if (
			recom.items[recom.items.length - 1].name !== '' &&
			recom.items[recom.items.length - 1].quantity !== 0
		) {
			let itemsCopy = recom.items
			itemsCopy.push(initialState.items[0])
			setRecom({
				...recom,
				items: itemsCopy
			})
		}
	}

	const inputHandler = ({ target: { value } }) => {
		setRecom({
			...recom,
			district: value
		})
	}

	const inputHandlerItem = (index, name, value) => {
		let itemsCopy = recom.items
		itemsCopy[index][name] = value
		setRecom({
			...recom,
			items: itemsCopy
		})
	}

	const submitHandler = event => {
		event.preventDefault()
		expertAddReport(recom.district, recom.items.slice(0, -1))
	}

	return (
		<Grid className={classes.root} container spacing={0}>
			<Typography variant='h4' component='h1' className={classes.heading}>
				Report Form
			</Typography>
			<form noValidate autoComplete='off' onSubmit={submitHandler}>
				{recom.items.map((item, key) => (
					<Fragment key={key}>
						<TextField
							name='name'
							label='Item Name'
							variant='outlined'
							autoFocus
							value={item.name}
							onChange={event =>
								inputHandlerItem(key, event.target.name, event.target.value)
							}
						/>
						{'  '}
						<TextField
							name='quantity'
							label='Quanitity per student in grams'
							type='number'
							variant='outlined'
							value={item.quantity}
							onChange={event =>
								inputHandlerItem(key, event.target.name, event.target.value)
							}
						/>
						<br />
						<br />
					</Fragment>
				))}
				<Button onClick={addItem}>Add Item</Button>
				<br />
				<br />
				<FormControl className={classes.formControl}>
					<InputLabel id='demo-simple-select-outlined-label'>
						District
					</InputLabel>
					<Select
						name='district'
						labelId='demo-simple-select-outlined-label'
						id='demo-simple-select-outlined'
						value={recom.district}
						onChange={inputHandler}
					>
						{districts.map((district, key) => (
							<MenuItem value={district} key={key}>
								{district}
							</MenuItem>
						))}
					</Select>
				</FormControl>
				<br />
				<br />
				<Input type='submit' value='Send Recommendation' />
			</form>
		</Grid>
	)
}

const mapStateToProps = state => ({
	expert: state.expert
})

export default connect(mapStateToProps, { expertAddReport, expertClearError })(Report)
