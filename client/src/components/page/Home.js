import React from 'react'
import { Link } from 'react-router-dom'
import { Grid, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
	root: {
		alignContent: 'center',
		flexGrow: 1,
		justifyContent: 'center',
		height: '100%'
	},
	item: {
		color: theme.palette.text.primary,
		padding: theme.spacing(2),
		textAlign: 'center'
	}
}))

const Home = () => {
	const classes = useStyles()

	return (
		<Grid className={classes.root} container spacing={0}>
			<Grid className={classes.item} item xs={2}>
				<Link to='/admin/login'>
					<Typography variant='h3' component='h1'>
						Admin Login
					</Typography>
				</Link>
			</Grid>
			<Grid className={classes.item} item xs={2}>
				<Link to='/expert/login'>
					<Typography variant='h3' component='h1'>
						Expert Login
					</Typography>
				</Link>
			</Grid>
			<Grid className={classes.item} item xs={2}>
				<Link to='/school/login'>
					<Typography variant='h3' component='h1'>
						School Login
					</Typography>
				</Link>
			</Grid>
			<Grid className={classes.item} item xs={2}>
				<Link to='/official/login'>
					<Typography variant='h3' component='h1'>
						Official Login
					</Typography>
				</Link>
			</Grid>
			<Grid className={classes.item} item xs={2}>
				<Link to='/supplier/login'>
					<Typography variant='h3' component='h1'>
						Supplier Login
					</Typography>
				</Link>
			</Grid>
		</Grid>
	)
}

export default Home
