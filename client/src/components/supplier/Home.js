import React from 'react'
import {
	Card,
	CardActions,
	CardContent,
	Button,
	Grid,
	Typography
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const orders = [
	{
		district: 'Alappuzha',
		items: [
			{ name: 'Dal', quantity: 20 },
			{ name: 'Green Peas', quantity: 20 }
		]
	}
]

const useStyles = makeStyles(theme => ({
	card: {
		color: theme.palette.text.secondary
	},
	cardButton: {
		color: theme.palette.text.secondary
	},
	heading2: {
		margin: '60px 0px 24px 0px'
	},
	root: {
		margin: '0px',
		width: 'calc(100% - ' + theme.spacing(3) / 2 + 'px)'
	}
}))

const Home = () => {
	const classes = useStyles()

	return (
		<>
			<Typography variant='h2' component='h1'>
				Orders
			</Typography>
			<Grid container spacing={3} className={classes.root}>
				{orders.map(({ district, items }, key) => (
					<Grid item xs={4} key={key}>
						<Card className={classes.card}>
							<CardContent>
								<Typography variant='h4' component='h2'>
									{district}
								</Typography>
								{items.map(({ name, quantity }, key2) => (
									<p key={key2}>
										{name} - {quantity}
									</p>
								))}
							</CardContent>
							<CardActions>
								<Button className={classes.cardButton} size='small'>
									Approve
								</Button>
							</CardActions>
						</Card>
					</Grid>
				))}
			</Grid>
			<Typography variant='h4' component='h2' className={classes.heading2}>
				Approved Orders
			</Typography>
			<Grid container spacing={3} className={classes.root}>
				{orders.map(({ district, items }, key) => (
					<Grid item xs={4} key={key}>
						<Card className={classes.card}>
							<CardContent>
								<Typography variant='h4' component='h2'>
									{district}
								</Typography>
								{items.map(({ name, quantity }, key2) => (
									<p key={key2}>
										{name} - {quantity}
									</p>
								))}
							</CardContent>
						</Card>
					</Grid>
				))}
			</Grid>
		</>
	)
}

export default Home
