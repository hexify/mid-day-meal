import React, { useState } from 'react'
import { Grid, Input, TextField, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'

import { supplierLogin } from '../../actions/supplierActions'

const useStyles = makeStyles(theme => ({
	root: {
		alignContent: 'center',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		height: '100%',
		textAlign: 'center'
	},
	heading: {
		marginBottom: '36px'
	}
}))

const Login = ({ history, supplierLogin }) => {
	const classes = useStyles()
	const initialState = {
		email: '',
		password: ''
	}

	const [user, setUser] = useState(initialState)

	const inputHandler = ({ target }) => {
		setUser({
			...user,
			[target.name]: target.value
		})
	}

	const submitHandler = event => {
		event.preventDefault()
		supplierLogin({ email: user.email, password: user.password })
	}

	return (
		<Grid className={classes.root} container spacing={0}>
			<Typography variant='h4' component='h1' className={classes.heading}>
				Supplier Login
			</Typography>
			<form noValidate autoComplete='off' onSubmit={submitHandler}>
				<TextField
					name='email'
					label='Email'
					variant='outlined'
					autoFocus
					value={user.email}
					onChange={inputHandler}
				/>
				<br />
				<br />
				<TextField
					name='password'
					label='Password'
					type='password'
					variant='outlined'
					value={user.password}
					onChange={inputHandler}
				/>
				<br />
				<br />
				<Input type='submit' value='Login' />
			</form>
		</Grid>
	)
}

export default connect(null, { supplierLogin })(Login)
