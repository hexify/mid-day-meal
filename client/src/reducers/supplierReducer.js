import { SUPPLIER_ERROR, SUPPLIER_LOADING, SUPPLIER_LOGIN } from '../actions/types'

const initialState = {
	alert: null,
	email: null,
	isAuth: false,
	loading: false,
	token: null
}

export default (state = initialState, action) => {
	switch (action.type) {
		case SUPPLIER_ERROR:
			return {
				...state,
				alert: action.payload,
				loading: false
			}
		case SUPPLIER_LOADING:
			return {
				...state,
				loading: true
			}
		case SUPPLIER_LOGIN:
			return {
				...state,
				email: action.payload.email,
				isAuth: true,
				loading: false,
				token: action.payload.token
			}
		default:
			return state
	}
}
