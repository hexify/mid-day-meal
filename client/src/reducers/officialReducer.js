import { OFFICIAL_ERROR, OFFICIAL_LOADING, OFFICIAL_LOGIN } from '../actions/types'

const initialState = {
	alert: null,
	email: null,
	isAuth: false,
	loading: false,
	token: null
}

export default (state = initialState, action) => {
	switch (action.type) {
		case OFFICIAL_ERROR:
			return {
				...state,
				alert: action.payload,
				loading: false
			}
		case OFFICIAL_LOADING:
			return {
				...state,
				loading: true
			}
		case OFFICIAL_LOGIN:
			return {
				...state,
				email: action.payload.email,
				isAuth: true,
				loading: false,
				token: action.payload.token
			}
		default:
			return state
	}
}
