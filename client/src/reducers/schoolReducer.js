import { SCHOOL_ERROR, SCHOOL_LOADING, SCHOOL_LOGIN } from '../actions/types'

const initialState = {
	alert: null,
	email: null,
	isAuth: false,
	loading: false,
	token: null
}

export default (state = initialState, action) => {
	switch (action.type) {
		case SCHOOL_ERROR:
			return {
				...state,
				alert: action.payload,
				loading: false
			}
		case SCHOOL_LOADING:
			return {
				...state,
				loading: true
			}
		case SCHOOL_LOGIN:
			return {
				...state,
				email: action.payload.email,
				isAuth: true,
				loading: false,
				token: action.payload.token
			}
		default:
			return state
	}
}
