import {
	ADMIN_ERROR,
	ADMIN_LOADING,
	ADMIN_LOGIN,
	ADMIN_ADD,
	ADMIN_ADD_ERROR,
	ADMIN_CLEAR_ERROR,
	ADMIN_LIST,
	ADMIN_EDIT,
	ADMIN_EDIT_ERROR
} from '../actions/types'

const initialState = {
	alert: null,
	email: null,
	isAuth: false,
	loading: false,
	token: null,
	users: []
}

export default (state = initialState, action) => {
	switch (action.type) {
		case ADMIN_ERROR:
			return {
				...state,
				alert: action.payload,
				loading: false
			}
		case ADMIN_LOADING:
			return {
				...state,
				loading: true
			}
		case ADMIN_LOGIN:
			return {
				...state,
				email: action.payload.email,
				isAuth: true,
				loading: false,
				token: action.payload.token
			}
		case ADMIN_ADD:
			return {
				...state,
				alert: 'User added!'
			}
		case ADMIN_ADD_ERROR:
			return {
				...state,
				alert: action.payload
			}
		case ADMIN_CLEAR_ERROR:
			return {
				...state,
				alert: null
			}
		case ADMIN_LIST:
			return {
				...state,
				users: action.payload
			}
		case ADMIN_EDIT:
			return {
				...state,
				alert: 'User edited!'
			}
		case ADMIN_EDIT_ERROR:
			return {
				...state,
				alert: action.payload
			}
		default:
			return state
	}
}
