import { combineReducers } from 'redux'

import adminReducer from './adminReducer'
import expertReducer from './expertReducer'
import officialReducer from './officialReducer'
import schoolReducer from './schoolReducer'
import supplierReducer from './supplierReducer'

export default combineReducers({
	admin: adminReducer,
	expert: expertReducer,
	official: officialReducer,
	school: schoolReducer,
	supplier: supplierReducer
})
