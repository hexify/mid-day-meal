import {
	EXPERT_ERROR, EXPERT_LOADING, EXPERT_LOGIN,
	EXPERT_ADD_REPORT, EXPERT_ADD_REPORT_ERROR, EXPERT_CLEAR_ERROR
} from '../actions/types'

const initialState = {
	alert: null,
	email: null,
	isAuth: false,
	loading: false,
	token: null
}

export default (state = initialState, action) => {
	switch (action.type) {
		case EXPERT_ERROR:
			return {
				...state,
				alert: action.payload,
				loading: false
			}
		case EXPERT_LOADING:
			return {
				...state,
				loading: true
			}
		case EXPERT_LOGIN:
			return {
				...state,
				email: action.payload.email,
				isAuth: true,
				loading: false,
				token: action.payload.token
			}
		case EXPERT_ADD_REPORT:
			return {
				...state,
				alert: 'Report added'
			}
		case EXPERT_ADD_REPORT_ERROR:
			return {
				...state,
				alert: action.payload
			}
		case EXPERT_CLEAR_ERROR:
			return{
				...state,
				alert: null
			}
		default:
			return state
	}
}
