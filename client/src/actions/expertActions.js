import {
	EXPERT_LOGIN, EXPERT_ERROR, EXPERT_LOADING, EXPERT_ADD_REPORT, EXPERT_ADD_REPORT_ERROR,
	EXPERT_CLEAR_ERROR
} from './types'
import Axios from '../utils/Axios'

export const expertLogin = ({ email, password }) => async dispatch => {
	try {
		dispatch(expertLoading())
		const res = await Axios('post', '/api/expert', { email, password })
		localStorage.setItem('token', res.data.token)
		dispatch({
			type: EXPERT_LOGIN,
			payload: {
				token: res.data.token,
				email: email
			}
		})
	} catch (err) {
		localStorage.removeItem('token')
		dispatch({
			type: EXPERT_ERROR,
			payload: err.response ? err.response.data.msg : 'Login failed'
		})
	}
}

export const expertLoading = () => async dispatch =>
	dispatch({ type: EXPERT_LOADING })

export const expertAddReport = (district, items, id) => async dispatch => {
	try {
		await Axios('post', '/api/expert/additems', { district, items, id})
		dispatch({ type: EXPERT_ADD_REPORT })
	}
	catch (err) {
		dispatch({
			type: EXPERT_ADD_REPORT_ERROR,
			payload: err.response ? err.response.data.msg : 'Add report failed'
		})
	}
}

export const expertClearError = () => async dispatch =>
	dispatch({ type: EXPERT_CLEAR_ERROR })