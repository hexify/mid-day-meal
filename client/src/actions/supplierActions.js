import { SUPPLIER_LOGIN, SUPPLIER_ERROR, SUPPLIER_LOADING} from './types'
import axios from 'axios'

export const supplierLogin = ({email, password}) => async dispatch => {

    const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	try {
		dispatch(supplierLoading())	
		const res = await axios.post('/api/supplier', {email, password}, config)
		localStorage.setItem('token', res.data.token)
		dispatch({
			type: SUPPLIER_LOGIN,
			payload: {
				token: res.data.token,
				email: email
			}
		})
	} catch (err) {
		localStorage.removeItem('token')
		dispatch({
			type: SUPPLIER_ERROR,
			payload: err.response ? err.response.data.msg : 'Login failed'
		})
	}
}

export const supplierLoading = () => async dispatch =>
	dispatch({ type: SUPPLIER_LOADING })