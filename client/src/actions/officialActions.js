import { OFFICIAL_LOGIN, OFFICIAL_ERROR, OFFICIAL_LOADING} from './types'
import axios from 'axios'

export const officialLogin = ({email, password}) => async dispatch => {

    const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	try {
		dispatch(officialLoading())	
		const res = await axios.post('/api/official', {email, password}, config)
		localStorage.setItem('token', res.data.token)
		dispatch({
			type: OFFICIAL_LOGIN,
			payload: {
				token: res.data.token,
				email: email
			}
		})
	} catch (err) {
		localStorage.removeItem('token')
		dispatch({
			type: OFFICIAL_ERROR,
			payload: err.response ? err.response.data.msg : 'Login failed'
		})
	}
}

export const officialLoading = () => async dispatch =>
	dispatch({ type: OFFICIAL_LOADING })