import Axios from '../utils/Axios'

import {
	ADMIN_ERROR,
	ADMIN_LOADING,
	ADMIN_LOGIN,
	ADMIN_ADD,
	ADMIN_ADD_ERROR,
	ADMIN_CLEAR_ERROR,
	ADMIN_LIST,
	ADMIN_EDIT,
	ADMIN_EDIT_ERROR
} from './types'

export const adminLogin = ({ email, password }) => async dispatch => {
	try {
		dispatch(adminLoading())
		const res = await Axios('post', '/api/admin', { email, password })
		localStorage.setItem('token', res.data.token)
		dispatch({
			type: ADMIN_LOGIN,
			payload: {
				token: res.data.token,
				email: email
			}
		})
	} catch (err) {

		console.log(err	)

		localStorage.removeItem('token')
		dispatch({
			type: ADMIN_ERROR,
			payload: err.response ? err.response.data.msg : 'Login failed'
		})
	}
}

export const adminLoading = () => async dispatch =>
	dispatch({ type: ADMIN_LOADING })

export const addUser = ({
	email,
	password,
	type,
	district,
	name
}) => async dispatch => {
	let api
	switch (type) {
		case 'Expert':
			api = '/api/expert/new'
			break
		case 'Official':
			api = '/api/official/new'
			break
		case 'School':
			api = '/api/school/new'
			break
		case 'Supplier':
			api = '/api/supplier/new'
			break
		default:
			break
	}
	try {
		await Axios('post', api, { email, password, type, district, name })
		dispatch({ type: ADMIN_ADD })
	} catch (err) {
		dispatch({
			type: ADMIN_ADD_ERROR,
			payload: err.response ? err.response.data.msg : 'Cannot add new user'
		})
		console.log(err)
	}
}

export const adminClearError = () => async dispatch =>
	dispatch({ type: ADMIN_CLEAR_ERROR })

export const getList = () => async dispatch => {
	try {
		let users = []
		let res = await Axios('get', '/api/expert/')
		res.data.map(user => users.push({ ...user, type: 'Expert' }))
		res = await Axios('get', '/api/school/')
		res.data.map(user => users.push({ ...user, type: 'School' }))
		res = await Axios('get', '/api/official/')
		res.data.map(user => users.push({ ...user, type: 'Official' }))
		res = await Axios('get', '/api/supplier/')
		res.data.map(user => users.push({ ...user, type: 'Supplier' }))
		dispatch({ type: ADMIN_LIST, payload: users })
	} catch (err) {
		dispatch({
			type: ADMIN_ADD_ERROR,
			payload: err.response ? err.response.data.msg : 'Cannot get list'
		})
		console.log(err)
	}
}

export const editUser = ({
	id,
	email,
	password,
	type,
	name
}) => async dispatch => {
	let api
	switch (type) {
		case 'Expert':
			api = '/api/expert/'
			break
		case 'Official':
			api = '/api/official/'
			break
		case 'School':
			api = '/api/school/'
			break
		case 'Supplier':
			api = '/api/supplier/'
			break
		default:
			break
	}
	try {
		await Axios('put', api + id, { email, password, type, name })
		dispatch({ type: ADMIN_EDIT })
	} catch (err) {
		dispatch({
			type: ADMIN_EDIT_ERROR,
			payload: err.response ? err.response.data.msg : 'Cannot edit user'
		})
		console.log(err)
	}
}
