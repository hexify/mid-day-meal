import { SCHOOL_LOGIN, SCHOOL_ERROR, SCHOOL_LOADING} from './types'
import axios from 'axios'

export const schoolLogin = ({email, password}) => async dispatch => {

    const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	try {
		dispatch(schoolLoading())	
		const res = await axios.post('/api/school', {email, password}, config)
		localStorage.setItem('token', res.data.token)
		dispatch({
			type: SCHOOL_LOGIN,
			payload: {
				token: res.data.token,
				email: email
			}
		})
	} catch (err) {
		localStorage.removeItem('token')
		dispatch({
			type: SCHOOL_ERROR,
			payload: err.response ? err.response.data.msg : 'Login failed'
		})
	}
}

export const schoolLoading = () => async dispatch =>
	dispatch({ type: SCHOOL_LOADING })