import axios from 'axios'

import setAuthToken from './setAuthToken'

const Axios = async (method, api, value) => {
	if (localStorage.token) {
		setAuthToken(localStorage.token)
	} else {
		return
	}

	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}
	return await axios[method](api, { ...value }, config)
}

export default Axios
