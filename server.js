const express = require('express')
const connectDB = require('./config/db')
const path = require('path')

const app = express()
const PORT = process.env.PORT || 5000

connectDB()

app.use(express.json({ extended: false }))

app.use('/api/auth', require('./routes/auth'))
app.use('/api/admin', require('./routes/admin'))
app.use('/api/expert', require('./routes/expert'))
app.use('/api/healthData', require('./routes/healthData'))
app.use('/api/official', require('./routes/official'))
app.use('/api/order', require('./routes/order'))
app.use('/api/school', require('./routes/school'))
app.use('/api/student', require('./routes/student'))
app.use('/api/supplier', require('./routes/supplier'))

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'))
	app.get('*', (req, res) =>
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
	)
}

let server = app.listen(PORT, () => console.log(`Server started on ${PORT}`))
